require 'rails_helper'

describe 'Home' do
    specify '画面の表示' do
        visit '/'
        expect(page).to have_title 'Ruby on Rails Tutorial Sample App'
    end
end

describe 'Help' do
    specify '画面の表示' do
        visit '/help'
        expect(page).to have_title 'Help | Ruby on Rails Tutorial Sample App'
    end
end

describe 'About' do
    specify '画面の表示' do
        visit '/about'
        expect(page).to have_title 'About | Ruby on Rails Tutorial Sample App'
    end
end

describe 'Contact' do
    specify '画面の表示' do
        visit '/contact'
        expect(page).to have_title 'Contact | Ruby on Rails Tutorial Sample App'
    end
end